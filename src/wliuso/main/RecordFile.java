package wliuso.main;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;

import wliuso.main.Log.Direction;


public class RecordFile {
	
	//Static Values and Paths
	public static Image omni = new ImageIcon(RecordFile.class.getResource("/icons/omni.jpg")).getImage();
	public static Image plusRow = new ImageIcon(RecordFile.class.getClass().getResource("/icons/addRow.png")).getImage();
	
	private String settings = "src\\settings";
	public String getSettings(){return settings;}
	public void setSettings(String inVal){settings = inVal;}
	
	private int refresh = 60000;
	public int getRefresh(){return refresh;}
	public void setRefresh(int inVal){refresh = inVal;}
	
	//Adjustable Values and Path for Logging
	private Inventory inventory = new Inventory();
	public Inventory getInventory(){return inventory;}
	public void setInventory(Inventory inVal){inventory = inVal;}
	public void addItem(Item inItem) {inventory.addItem(inItem);}
	
	Set<Log> logList =  new HashSet<>();
	public Set<Log> getLogList(){return logList;}
	public void setLogList(Set<Log> inVal){logList = inVal;}
	public void addLog(Log inLog) {logList.add(inLog);}
	
	private String record = "files\\record.xml";
	public String getRecord(){return record;}
	public void setRecord(String inVal){
		record = inVal;
		saveSettings();
	}
	
	//Load Settings
	public void loadSettings(){
		BufferedReader reader = null;
		try{
			String inLine;
			reader = new BufferedReader(new FileReader(settings));
			while ((inLine = reader.readLine()) != null){
				if(inLine.contains("Record=")){
					record = inLine.substring(7);
				}else{
					//What
				}
			}
			reader.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	//Save Settings
	public void saveSettings(){
		PrintWriter os;
		try {
			os = new PrintWriter(settings);
			os.println("Record=" + record);
			os.close();
		}catch (FileNotFoundException e1) {
				e1.printStackTrace();
		}
	}

	public boolean saveTest(){
		Set<Log> newLoad =  new HashSet<>();
		newLoad = logList;
		Set<Log> oldLoad =  InventoryManager.checkLogs;
		boolean allGood = true;
		int goodCount = 0;
		if(newLoad.size() != oldLoad.size()) {
			allGood = false;
			//System.out.println("Error F01: " + oldLoad.size() + " : " + newLoad.size());
		}else{
			//System.out.println("Size Check Passed. Beginning individual check.");
			Iterator<Log> newItr = newLoad.iterator();
			while(newItr.hasNext()) {
				Iterator<Log> oldItr = oldLoad.iterator();
				Log newLog = newItr.next();
				//System.out.println("~~Checking new log: " + newLog.getDate() + " - " + newLog.getLocation() + " - " + newLog.displayItems() + " - " + newLog.getDirection());
				while(oldItr.hasNext()) {
					Log oldLog = oldItr.next();
					//System.out.println("Comparing old log: " + oldLog.getDate() + " - " + oldLog.getLocation() + " - " + oldLog.displayItems() + " - " + oldLog.getDirection());
					if(!newLog.getLocation().equalsIgnoreCase(oldLog.getLocation())) {
						//System.out.println("Error F02: " + newLog.getLocation() + " - " + oldLog.getLocation());
					}else if(!newLog.getDate().equalsIgnoreCase(oldLog.getDate())){
						//System.out.println("Error F03: " + newLog.getDate() + " - " + oldLog.getDate());
					}else if(newLog.getDirection() != oldLog.getDirection()) {
						//System.out.println("Error F04: " + newLog.getDirection() + " - " + oldLog.getDirection());
					}else if(!newLog.displayItems().equalsIgnoreCase(oldLog.displayItems())){
						//System.out.println("Error F05: " + newLog.displayItems() + " - " + oldLog.displayItems());
					}else {
						goodCount++;
						//System.out.println("Success " + goodCount + "/" + oldLoad.size());
					}
				}
				
			}
		}
		//System.out.println(goodCount + " : " + newLoad.size());
		if(goodCount != newLoad.size()) {
			allGood = false;
		}
		return allGood;
	}

	public void save() {
		PrintWriter os;
		boolean canSave = saveTest();
		if(canSave){
			try {
				os = new PrintWriter(getRecord());
				//Save Item List
				Iterator<Item> itemItr = inventory.getItems().iterator();
				os.println("===BEGIN ITEM SECTION===");
				while(itemItr.hasNext()) {
					os.println("<item>" + itemItr.next().getName() + "</item>");
					//System.out.println(itemItr.next());
				}
				//Save Transaction Logs
				os.println("===BEGIN TRANSACTION SECTION===");
				for(Log thisLog: logList){
					os.print("<transaction>" + thisLog.getDate() + ",");
					os.print(thisLog.getLocation() + ",");
					os.print(thisLog.saveItems() + ",");
					os.print(thisLog.getDirection() + "</transaction>\n");
				}	
				//Save SIMS
				os.println("===BEGIN SIM CARD SECTION===");

				os.close();
			}catch (FileNotFoundException e1) {
					e1.printStackTrace();
			}
		}
	}
	
	public void load(){
		Pattern itemPattern = Pattern.compile("(<item>)(.+)(</item>)");
		Pattern logPattern = Pattern.compile("(<transaction>)(.+)(</transaction>)");
		BufferedReader reader = null;
		try{
			String inLine;
			reader = new BufferedReader(new FileReader(getRecord()));
			while ((inLine = reader.readLine()) != null){
				Matcher itemMatcher = itemPattern.matcher(inLine);
				Matcher logMatcher = logPattern.matcher(inLine);
				if(itemMatcher.matches()){
					Item newItem = new Item(itemMatcher.group(2));
					addItem(newItem);
				}else if(logMatcher.matches()){
					String[] logVals = logMatcher.group(2).split(",");
					String thisDate = logVals[0];
					String thisLocation = logVals[1];
					ArrayList<Transaction> theseItems = new ArrayList<>();
					String dirString = logVals[3];
					String itemsWhole = logVals[2].substring(1, logVals[2].length()-1);
					String[] itemsHalf = itemsWhole.split("\\]\\[");
					for(String item: itemsHalf){
						String[] oneItem = item.split(";");
						Direction thisDir = Direction.valueOf(dirString.toUpperCase());
						Item thisItem = new Item(oneItem[0]);
						Transaction thisTran = new Transaction(thisDir, thisItem, Integer.parseInt(oneItem[1]));
						theseItems.add(thisTran);
					}
					Log thisEntry = new Log(Direction.valueOf(dirString.toUpperCase()), thisLocation, thisDate, theseItems);
					addLog(thisEntry);
				}
			}
			reader.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void saveNew(String record) {
		PrintWriter os;
		try {
			os = new PrintWriter(record);
			//Save Item List
			Iterator<Item> itemItr = inventory.getItems().iterator();
			os.println("===BEGIN ITEM SECTION===");
			while(itemItr.hasNext()) {
				os.println("<item>" + itemItr.next().getName() + "</item>");
				//System.out.println(itemItr.next());
			}
			//Save Transaction Logs
			os.println("===BEGIN TRANSACTION SECTION===");
			for(Log thisLog: logList){
				os.print("<transaction>" + thisLog.getDate() + ",");
				os.print(thisLog.getLocation() + ",");
				os.print(thisLog.saveItems() + ",");
				os.print(thisLog.getDirection() + "</transaction>\n");
			}	
			//Save SIMS
			os.println("===BEGIN SIM CARD SECTION===");
			os.close();
		}catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}
