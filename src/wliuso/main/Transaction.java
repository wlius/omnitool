package wliuso.main;
import wliuso.main.Log.Direction;

//This class is for individual transactions for individual items.

public class Transaction {
	private Direction direction;
	public Direction getDir() {return direction;}
	public void setDir(Direction thisDir) {this.direction = thisDir;}
	
	private Item item;
	public Item getItem(){return item;}
	public void setItem(Item inVal){item = inVal;}
	
	private int number;
	public int getNumber(){return number;}
	public void setNumber(int inVal){number = inVal;}

	public Transaction(){
		direction = null;
		item = null;
		number = 0;
	}
	
	public Transaction(Direction inDir, Item inItem, int inNum){
		direction = inDir;
		item = inItem;
		number = inNum;
	}
}
