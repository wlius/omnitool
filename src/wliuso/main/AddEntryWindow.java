package wliuso.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import wliuso.main.InventoryManager.InvModel;
import wliuso.main.InventoryManager.LogModel;
import wliuso.main.Log.Direction;

@SuppressWarnings("serial")
public class AddEntryWindow extends JFrame  {

	int itemNum = 0;
	
	public AddEntryWindow(InvModel invModel, LogModel logModel, RecordFile inFile){
		JPanel addPane = new JPanel(new GridBagLayout());
		setTitle("Add Log Entry");
		setIconImage(RecordFile.omni);
		Image addImg = new ImageIcon(this.getClass().getResource("/icons/addRow.png")).getImage();
		//Gridbag Constraints for Alteration Section
		GridBagConstraints labels = new GridBagConstraints();
			labels.gridx = 0;labels.gridy = 0;
			labels.insets = new Insets(10,10,0,10);
			labels.anchor = GridBagConstraints.NORTH;
		GridBagConstraints entry = new GridBagConstraints();
			entry.gridx = 0;entry.gridy = 1;
			entry.anchor = GridBagConstraints.SOUTH;
		JLabel enterDir = new JLabel("In/Out");
		addPane.add(enterDir, labels);
			labels.gridx = 1;
		JLabel enterLocation = new JLabel("To/From");
		addPane.add(enterLocation, labels);
			labels.gridx = 2;
		JLabel enterDate = new JLabel("Date");
		addPane.add(enterDate, labels);
		String[] dirStrings = {"IN", "OUT"};
		JComboBox<String> dirBar = new JComboBox<String>(dirStrings);
		addPane.add(dirBar, entry);
			entry.gridx = 1;
		JTextField locationBar = new JTextField(20);
		addPane.add(locationBar, entry);
			entry.gridx = 2;
		JTextField dateBar = new JTextField(10);
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String dateString = localDate.format(formatter);
		dateBar.setText(dateString);
		addPane.add(dateBar, entry);
		GridBagConstraints labCons = new GridBagConstraints();
			labCons.gridx = 1; labCons.gridy = 3;
			labCons.insets = new Insets(20,10,0,10);
		JLabel itemNa = new JLabel("Name");
		addPane.add(itemNa, labCons);
			labCons.gridx = 2; 
		//Button to Add Additional Item Input Row
		JButton addItem = new JButton(new ImageIcon(addImg.getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
		addItem.setBorder(BorderFactory.createEmptyBorder());
		addItem.setContentAreaFilled(false);
		JLabel itemQu = new JLabel("Quantity");
		itemQu.setPreferredSize(new Dimension(80,25));
		FlowLayout newLayout = new FlowLayout();
		newLayout.setAlignment(FlowLayout.RIGHT);
		itemQu.setLayout(newLayout);
		itemQu.add(addItem, newLayout);
		addPane.add(itemQu, labCons);
		GridBagConstraints inCons = new GridBagConstraints();
			inCons.gridy = 4; inCons.anchor = GridBagConstraints.CENTER;
		ArrayList<JComboBox<String>> itemBoxList = new ArrayList<>();
		ArrayList<JTextField> quantityBoxList = new ArrayList<>();
		newRow(inCons, addPane, labCons, itemBoxList, quantityBoxList, addItem, inFile.getInventory());
		GridBagConstraints subCons = new GridBagConstraints();
			subCons.gridx = 0; subCons.gridy = itemNum+4; subCons.insets = new Insets(10,10,10,10);
		JLabel success = new JLabel("");
		success.setMinimumSize(new Dimension(70,16));
		success.setPreferredSize(new Dimension(70,16));
		addPane.add(success, subCons);
			subCons.gridx = 1;
		JButton submit = new JButton("Submit");
		addPane.add(submit, subCons);
		submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	ArrayList<String> itemList = new ArrayList<>();
        		ArrayList<Integer> quantityList = new ArrayList<>();
        		for(JComboBox<String> item: itemBoxList){
        			if(!item.getSelectedItem().toString().equalsIgnoreCase("No Item")){
        				itemList.add(item.getSelectedItem().toString());
        			}
        		}
        		Boolean validEntry = true;
        		Boolean errorFired = false;
        		if(locationBar.getText().isEmpty() && !errorFired){
        			JOptionPane.showMessageDialog(null, "Location is a required field.");
        			validEntry = false;
        			errorFired = true;
        		}
        		if(dateBar.getText().isEmpty() && !errorFired){
        			JOptionPane.showMessageDialog(null, "Date is a required field.");
        			validEntry = false;
        			errorFired = true;
        		}
        		for(int i = 0; i < itemBoxList.size(); i++){
        			if(itemBoxList.get(i).getSelectedItem().toString().equalsIgnoreCase("No Item") || quantityBoxList.get(i).getText().isEmpty()){
        				if(!errorFired){
        					JOptionPane.showMessageDialog(null, "Item names and quantities are not filled properly.");
                			validEntry = false;
                			errorFired = true;
        				}}}
        		for(JTextField quantity: quantityBoxList){
        			if(!quantity.getText().isEmpty()){
        				try{
        					quantityList.add(Integer.parseInt(quantity.getText()));
        				}catch (NumberFormatException heck){
        					success.setText("");
        					JOptionPane.showMessageDialog(null, "There is non-integer text in a quantity box.");
        					validEntry = false;
        				}}}
        		if(validEntry){
            		Log thisLog = new Log(Direction.valueOf(dirBar.getSelectedItem().toString()), locationBar.getText(), dateBar.getText());
            		for(int i = 0; i < itemList.size(); i++){
            			Transaction thisTrans = new Transaction(Direction.valueOf(dirBar.getSelectedItem().toString()), new Item(itemList.get(i)), quantityList.get(i));
            			thisLog.addTransaction(thisTrans);
            			inFile.getInventory().transaction(thisTrans);
            		}
            		inFile.getLogList().add(thisLog);
            		Object[] outLogRow = { thisLog.getDate(), thisLog.getLocation(), thisLog.displayItems(), thisLog.getDirection() };
            		logModel.addRow(outLogRow);
            		Set<Item> outItemList = inFile.getInventory().getInventory().keySet();
            		invModel.clear();
            		for(Item item: outItemList){
            			ArrayList<Object> workRow = new ArrayList<>();
            			workRow.add(item.getName());
            			workRow.add(inFile.getInventory().getNumber(item));
            			invModel.addRow(workRow.toArray());
            		}
            		for(JComboBox<String> item: itemBoxList){
            			item.setSelectedItem("No Item");
            		}
            		for(JTextField quantity: quantityBoxList){
            			quantity.setText("");
            		}
            		success.setText("Log Created");
        		}
        		inFile.setLogList(inFile.getLogList());
        		inFile.save();
        		invModel.fireTableDataChanged();
        		logModel.fireTableDataChanged();
            }
        });
		

		addItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPane.remove(addItem);
				addPane.remove(success);
				addPane.remove(submit);
				newRow(inCons, addPane, labCons, itemBoxList, quantityBoxList, addItem, inFile.getInventory());
					subCons.gridx = 0; subCons.gridy = itemNum+5; subCons.insets = new Insets(10,10,10,10);
				addPane.add(success, subCons);
					subCons.gridx = 1;
				addPane.add(submit, subCons);
					subCons.gridx = 2;
				addPane.revalidate();
				addPane.repaint();
				addPane.getParent().setSize(750,750);
				SwingUtilities.getWindowAncestor(addPane).pack();
				SwingUtilities.getWindowAncestor(addPane).setMinimumSize(getPreferredSize());
			}
		});
			subCons.gridx = 2;
		//addPane.add(addItem, subCons);
		getContentPane().add(addPane);
		SwingUtilities.getWindowAncestor(addPane).pack();
		SwingUtilities.getWindowAncestor(addPane).setMinimumSize(getPreferredSize());
	}
	
	public void newRow(GridBagConstraints inCons, JPanel addPane, GridBagConstraints labCons, ArrayList<JComboBox<String>> itemBoxList, ArrayList<JTextField> quantityBoxList, JButton addButton, Inventory inventory) {
		itemNum += 1;
		int thisItem = itemNum;
			inCons.gridx = 0;
		JLabel addItem = new JLabel("Item " + thisItem);
		addPane.add(addItem, inCons);
		labCons.anchor = GridBagConstraints.SOUTH;
			inCons.gridx = 1;
		//JTextField itemBar = new JTextField(14);
		JComboBox<String> itemBar = new JComboBox<>();
		itemBar.addItem("No Item");
		ArrayList<String> itemList = new ArrayList<>();
		for(Item item: inventory.getItems()) {
			itemList.add(item.getName());
		}
		java.util.Collections.sort(itemList);
		for(String item: itemList) {
			itemBar.addItem(item);
		}
		itemBoxList.add(itemBar);
		addPane.add(itemBar, inCons);
			inCons.gridx = 2;
		JTextField quantityBar = new JTextField(4);
		quantityBoxList.add(quantityBar);
		addPane.add(quantityBar, inCons);
		//addPane.add(addButton, inCons);
			inCons.gridy = itemNum+5;
	}
}
