package wliuso.main;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

@SuppressWarnings("serial")
public class InventoryManager extends JFrame {
	
	enum tableType { INV, LOG; }
	
	//Record File
	public RecordFile file = new RecordFile();
	
	//For Importing from Files and Refreshing
	public static Set<Log> checkLogs =  new HashSet<>();

	//For Tables
	private TableRowSorter<InvModel> invSort;
	private TableRowSorter<LogModel> logSort;
	//Create a tabbed pane and both tables
	InvModel invModel = new InvModel();
	LogModel logModel = new LogModel();
	
	//For GUI
	JTabbedPane displayPane = new JTabbedPane();
	JPanel additionPane = new JPanel(new GridBagLayout());
	JPanel windowBody = new JPanel(new GridBagLayout());
	JTextField searchBar = new JTextField(30);
	GridBagConstraints it = new GridBagConstraints();
	
	public InventoryManager(){
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		
		setTitle("WLIUS Omnitool");
		setIconImage(RecordFile.omni.getScaledInstance(32, 32, Image.SCALE_SMOOTH));
		file.loadSettings();
		Conversion.importInv(file);
		file.load();
		checkLogs = file.getLogList();
		file.getInventory().parseLogs(file.getLogList());
		
		//Gridbag Constraints for Buttons, Search Bar, and Table
		GridBagConstraints eb = new GridBagConstraints();
			eb.gridx = 0;
			eb.gridy = 0;
			eb.insets = new Insets(10,10,10,10);
			eb.anchor = GridBagConstraints.WEST;
		
		GridBagConstraints sb = new GridBagConstraints();
			sb.gridx = 0;
			sb.gridy = 1;
			sb.insets = new Insets(10,10,10,10);
			sb.fill = GridBagConstraints.BOTH;
			sb.anchor = GridBagConstraints.NORTHWEST;
			sb.weightx = 1.0;
			sb.weighty = 0.0;

		//Gridbag Constraints for Items Panel
			it.gridx = 0;
			it.gridy = 2;
			it.insets = new Insets(0,10,10,10);
			it.fill = GridBagConstraints.BOTH;
			it.anchor = GridBagConstraints.NORTHWEST;
			it.weightx = 1.0;
			it.weighty = 1.0;
			
		//Assorted Buttons Go Here
			JButton refresh = new JButton("Reload List");
			eb.anchor = GridBagConstraints.CENTER;
		
		invModel.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
			}
		});
		logModel.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
			}
		});
		
		JTable invTable = invTable(invModel);
		JTable logTable = logTable(logModel);
		
		logTable.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		    	if (me.getClickCount() == 2) {
		    		int thisRow = logTable.getRowSorter().convertRowIndexToModel(logTable.getSelectedRow());
		    		ArrayList<Object> rowData = new ArrayList<>();
		    		for(int i = 0; i < logModel.getColumnCount(); i++){
		    			rowData.add(logModel.getValueAt(thisRow, i));
		    		}
		    		Log editLog = new Log();
		    		for(Log log: file.getLogList()){
		    			if(log.matchesRow(rowData)){
		    				editLog = log;
		    			}
		    		}
		    		
		    		EditEntryWindow editWindow = new EditEntryWindow(invModel, logModel, editLog, file);
		    		editWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		    		editWindow.setMinimumSize(editWindow.getPreferredSize());
		    		editWindow.setSize(editWindow.getPreferredSize());
		    		editWindow.setVisible(true);
		    	}
		    }
		});
		
		JScrollPane tab1 = new JScrollPane();
		JScrollPane tab2 = new JScrollPane();
		tab1.add(invTable);tab1.setViewportView(invTable);
		tab2.add(logTable);tab2.setViewportView(logTable);
		
		// Tab1 - Item, Quantity in Stock
		String invTab = "Inventory";
		displayPane.addTab(invTab, tab1);
		// Tab2 - In/Out, Date, Item(s), Direction
		displayPane.addTab("Logs", tab2);
		
		displayPane.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		    	if (me.getClickCount() == 2) {
		    		int tabNum = displayPane.getSelectedIndex();
		    		switch(tabNum){
		    		case 0:
		    			itemWindow();
		    			break;
		    		case 1:
		    			addEntryWindow();
		    			break;
		    		}
		    	}
		    }
		});
		
		//Button Listeners
		refresh.addActionListener(new ActionListener() {
			@Override
	        public void actionPerformed(ActionEvent e) {
				refresh(invTable, logTable, file);
			}
		});
		
		//Repaint Table as You Type
		searchBar.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) {	
				sortFilter();
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				sortFilter();
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				sortFilter();
			}
        });
		
		//Start Refresh Timer
		Timer timer = new Timer(file.getRefresh(), new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		//Refresh the panel
			checkLogs = file.getLogList();
			invModel.fireTableDataChanged();
			logModel.fireTableDataChanged();
			//System.out.println("Refreshed.");
		}    
	});

		timer.start();
		
		//Add Everything to the Window
		//windowBody.add(refresh, eb);
		windowBody.add(searchBar, sb);
		windowBody.add(displayPane, it);
		windowBody.setMinimumSize(getPreferredSize());
		getContentPane().add(windowBody);
	}
	
	class InvModel extends AbstractTableModel {
		private String[] colHeads = {"Item","Quantity"};
		private Class<?>[] colTypes = {String.class, Integer.class};
		private ArrayList<Object[]> data = new ArrayList<>();
		@Override
		public int getColumnCount() {return colHeads.length;}
		@Override
		public int getRowCount() {return data.size();}
		public boolean isCellEditable(int row, int column){return false;}
		public void addRow(Object[] inRow){data.add(inRow);}
		public void removeRow(int inRow){data.remove(inRow);}
		public void clear(){data.clear();}
	    public String getColumnName(int inVal) { return colHeads[inVal];}
	    public Class<?> getColumnClass(int inVal) { return colTypes[inVal];}
	    @Override
		public Object getValueAt(int row, int col) {
	    	switch(col) {
	    	case 0:
	    		return file.getInventory().get(row).getKey().getName();
	    	case 1:
	    		return file.getInventory().get(row).getValue();
	    	default:
	    		return null;
	    	}
		}
	}
	
	class LogModel extends AbstractTableModel {
		private String[] colHeads = {"Date","To/From","Item(s)","In/Out"};
		private Class<?>[] colTypes = {String.class,String.class,String.class,Item.class};
		private ArrayList<Object[]> data = new ArrayList<>();
		@Override
		public int getColumnCount() {return colHeads.length;}
		@Override
		public int getRowCount() {return data.size();}
		public boolean isCellEditable(int row, int column){return false;}
		public void addRow(Object[] inRow){data.add(inRow);}
		public void removeRow(int inRow){data.remove(inRow);}
		public void clear(){data.clear();}
	    public String getColumnName(int inVal) { return colHeads[inVal];}
	    public Class<?> getColumnClass(int inVal) { return colTypes[inVal];}
		@Override
		public Object getValueAt(int row, int col) {
    	    Set<Log> logSet = file.getLogList();
    	    Iterator<Log> logItr = logSet.iterator();
    	    Log thisLog = new Log();
    	    Log outLog = new Log();
    	    int i = 0;
    	    while(logItr.hasNext() && i <= row) {
    	    	thisLog = logItr.next();
    	    	if(i==row) {
    	    		outLog = thisLog;
    	    	}
    	    	i++;
    	    }
			switch(col) {
	    	case 0:
	    	    return outLog.getDate();
	    	case 1:
	    		return outLog.getLocation();
	    	case 2:
	    	    return outLog.displayItems();
	    	case 3:
	    	    return outLog.getDirection();
	    	default:
	    		return null;
	    	}
		}
	}
	
	public JTable invTable(InvModel invModel){
		JTable returnTable = new JTable();
		Set<Item> itemList = file.getInventory().getInventory().keySet();
		for(Item item: itemList){
			ArrayList<Object> workRow = new ArrayList<>();
			workRow.add(item.getName());
			workRow.add(file.getInventory().getNumber(item));
			invModel.addRow(workRow.toArray());
		}
		List<RowSorter.SortKey> returnKeys = new ArrayList<>(25);
	    returnKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		invSort = new TableRowSorter<InvModel>(invModel);
	    returnTable.setRowSorter(invSort);
	    invSort.setSortKeys(returnKeys);
	    returnTable.setModel(invModel);
	    returnTable.setMinimumSize(getPreferredSize());
		return returnTable;
	}
	
	public JTable logTable(LogModel logModel){
		JTable returnTable = new JTable();
		for(Log entry: file.getLogList()){
			ArrayList<Object> workRow = new ArrayList<>();
			workRow.add(entry.getDate().toString());
			workRow.add(entry.getLocation());
			workRow.add(entry.displayItems());
			workRow.add(entry.getDirection().toString());
			logModel.addRow(workRow.toArray());
		}
		List<RowSorter.SortKey> returnKeys = new ArrayList<>(25);
	    returnKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		logSort = new TableRowSorter<LogModel>(logModel);
	    returnTable.setRowSorter(logSort);
	    logSort.setSortKeys(returnKeys);
	    returnTable.setModel(logModel);
	    returnTable.setMinimumSize(getPreferredSize());
		return returnTable;
	}
	
	private void sortFilter(){
		RowFilter<InvModel, Object> invFilter = null;
		RowFilter<LogModel, Object> logFilter = null;
		RowFilter<LogModel, Object> logFilter0 = null;
		RowFilter<LogModel, Object> logFilter1 = null;
		RowFilter<LogModel, Object> logFilter2 = null;
		RowFilter<LogModel, Object> logFilter3 = null;
		
		try{
			invFilter = RowFilter.regexFilter("(?i)" + searchBar.getText(), 0);
			//Sorting for Multiple Columns of Log Table
			List<RowFilter<LogModel, Object>> logFilterList = new ArrayList<RowFilter<LogModel, Object>>();
				logFilter0 = RowFilter.regexFilter("(?i)" + searchBar.getText(), 0);
				logFilterList.add(logFilter0);
				logFilter1 = RowFilter.regexFilter("(?i)" + searchBar.getText(), 1);
				logFilterList.add(logFilter1);
				logFilter2 = RowFilter.regexFilter("(?i)" + searchBar.getText(), 2);
				logFilterList.add(logFilter2);
				logFilter3 = RowFilter.regexFilter("(?i)" + searchBar.getText(), 3);
				logFilterList.add(logFilter3);
			logFilter = RowFilter.orFilter(logFilterList);
			
		}catch(java.util.regex.PatternSyntaxException e){return;}
		invSort.setRowFilter(invFilter);
		logSort.setRowFilter(logFilter);
	}

	public void addEntryWindow() {
		AddEntryWindow addWindow = new AddEntryWindow(invModel, logModel, file);
		addWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindow.setMinimumSize(addWindow.getPreferredSize());
		addWindow.setSize(addWindow.getPreferredSize());
		addWindow.setVisible(true);
	}
	
	public void itemWindow() {
		ItemWindow addWindow = new ItemWindow(invModel, logModel, file);
		addWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindow.setMinimumSize(addWindow.getPreferredSize());
		addWindow.setSize(addWindow.getPreferredSize());
		addWindow.setVisible(true);
	}
	
	public void refresh(JTable inv, JTable log, RecordFile file){
		file.load();
		file.getInventory().parseLogs(file.getLogList());
		checkLogs = file.getLogList();
		invModel.fireTableDataChanged();
		logModel.fireTableDataChanged();
	}
	
	//Main Method
	public static void main(String[] args) {
		InventoryManager InvWindow = new InventoryManager();
		InvWindow.setJMenuBar(MenuBar.buildMenu(InvWindow.file));
		InvWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		InvWindow.setMinimumSize(InvWindow.getPreferredSize());
		InvWindow.setSize(InvWindow.getPreferredSize());
		InvWindow.setVisible(true);
	}

}
