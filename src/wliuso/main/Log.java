package wliuso.main;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Log {
	enum Direction { IN, OUT, NULL; }
	private Direction direction;
	public Direction getDirection(){return direction;}
	public void setDirection(Direction inVal){direction = inVal;}
	
	private String location;
	public String getLocation(){return location;}
	public void setLocation(String inVal){location = inVal;}
	
	private DateFormat logDate = new SimpleDateFormat("MM/dd/yyyy");
	private Date date;
	public String getDate(){return logDate.format(date);}
	public void setDate(String inVal){try {date = logDate.parse(inVal);}
									  catch (ParseException e) {e.printStackTrace();}}
	
	private ArrayList<Transaction> transactions;
	public ArrayList<Transaction> getTransactions(){return transactions;}
	public void clearTransactions(){transactions.clear();}
	public void addTransaction(Transaction inVal){transactions.add(inVal);}
	public void remTransaction(Transaction inVal){transactions.remove(inVal);}
	
	public Log(Direction inDir, String inLocation, String inDate, ArrayList<Transaction> inTrans){
		direction = inDir;
		location = inLocation;
		try{date = logDate.parse(inDate);}catch (ParseException e) {e.printStackTrace();}
		transactions = inTrans;
	}
	
	public Log(Direction inDir, String inLocation, String inDate){
		direction = inDir;
		location = inLocation;
		try{date = logDate.parse(inDate);}catch (ParseException e) {e.printStackTrace();}
		transactions = new ArrayList<>();
	}
	
	public Log() {
		direction = Direction.NULL;
		location = "";
		date = null;
		transactions = new ArrayList<Transaction>();
	}
	public String displayItems(){
		StringBuilder outBuild = new StringBuilder();
		ArrayList<Transaction> theseTrans = getTransactions();
		Iterator<Transaction> transItr = theseTrans.iterator();
		while(transItr.hasNext()){
			Transaction thisTrans = transItr.next();
			outBuild.append(thisTrans.getItem().getName());
			outBuild.append(" (x" + thisTrans.getNumber() + ")");
			if(transItr.hasNext()) {
				outBuild.append(", ");
			}
		}
		return outBuild.toString();
	}

	public String saveItems(){
		StringBuilder saveItems = new StringBuilder();
		String outItems = "";
		ArrayList<Transaction> outTrans = getTransactions();
		Iterator<Transaction> transItr = outTrans.iterator();
		while(transItr.hasNext()) {
			Transaction thisTrans = transItr.next();
			saveItems.append("[");
			saveItems.append(thisTrans.getItem().getName());
			saveItems.append(";");
			saveItems.append(thisTrans.getNumber());
			saveItems.append("]");
		}
		outItems = saveItems.toString();
		return outItems;
	}
	public boolean matchesRow(ArrayList<Object> rowData) {
		if(!logDate.format(date).equalsIgnoreCase(rowData.get(0).toString())){
			return false;
		}else if(!location.equalsIgnoreCase(rowData.get(1).toString())){
			return false;
		}else if(!direction.equals(Direction.valueOf(rowData.get(3).toString()))){
			return false;
		}else{
			boolean itemsMatch = false;
			for(Transaction trans: transactions){
				if(rowData.get(2).toString().contains(trans.getItem().getName())){
					itemsMatch = true;
				}
			}
			if(itemsMatch){
				return true;
			}else{
				return false;
			}
		}
	}
}

//Tab2 - In/Out, Date, Item(s)