package wliuso.main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;


public class MenuBar {
	
	public static JMenuBar buildMenu(RecordFile inFile){
		
		JMenuBar outMenu = new JMenuBar();
		JMenu fileMenu, newMenu, settingsMenu, helpMenu;
		
		fileMenu = new JMenu("File");
			JMenuItem newFile = new JMenuItem("Create Record");
			newFile.addActionListener(new ActionListener() {
				@Override
		        public void actionPerformed(ActionEvent e) {
					JFileChooser saveWhere = new JFileChooser();
					FileNameExtensionFilter wlr = new FileNameExtensionFilter("Wireless Links Record File", "wlr");
					saveWhere.addChoosableFileFilter(wlr);
					int returnVal = saveWhere.showSaveDialog(fileMenu);
					if(returnVal == JFileChooser.APPROVE_OPTION){
						String filePath = saveWhere.getSelectedFile().getPath();
						if(!filePath.endsWith(".xml")){
							filePath = filePath + ".xml";
						}
						inFile.saveNew(filePath);
					}
				}
			});
			fileMenu.add(newFile);
			
			//fileMenu.addSeparator();
			//JMenuItem importFile = new JMenuItem("Import Logs");
				//TODO On-click
				//importLogs();
			//fileMenu.add(importFile);
			JMenuItem exportFile = new JMenuItem("Export Logs");
				//TODO On-click
				//exportLogs();
			//fileMenu.add(exportFile);
			//fileMenu.addSeparator();
		
		newMenu = new JMenu("Create");
			JMenuItem newItem = new JMenuItem("New Item");
			
			JMenuItem newEntry = new JMenuItem("New Entry");
			newEntry.addActionListener(new ActionListener() {
				@Override
		        public void actionPerformed(ActionEvent e) {
					//InventoryManager.addWindow();
				}
			});
			
		settingsMenu = new JMenu("Settings");
			JMenuItem saveFile = new JMenuItem("File Settings");
			saveFile.addActionListener(new ActionListener() {
				@Override
		        public void actionPerformed(ActionEvent e) {
					FileWindow fileWindow = new FileWindow(inFile, outMenu);
					fileWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					fileWindow.setMinimumSize(fileWindow.getPreferredSize());
					fileWindow.setSize(fileWindow.getPreferredSize());
					fileWindow.setVisible(true);
				}
			});
			settingsMenu.add(saveFile);
			
		helpMenu = new JMenu("Help");
			JMenuItem guidePop = new JMenuItem("Guide");
				//TODO Add Guide Window
			helpMenu.add(guidePop);
			JMenuItem helpPop = new JMenuItem("About");
				//TODO Adding Importing
			helpMenu.add(helpPop);
			
		outMenu.add(fileMenu);
		//outMenu.add(newMenu);
		outMenu.add(settingsMenu);
		//outMenu.add(helpMenu);
		
		return outMenu;
	}
	private static void importLogs(){
		//TODO Add Importing
	}
	private static void exportLogs(){
		//TODO Add Exporting
	}
}
