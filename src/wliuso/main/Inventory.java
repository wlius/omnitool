package wliuso.main;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

//This class will map Items with Transactions

public class Inventory {
	
	private HashMap<Item, Integer> inventory = new HashMap<>();
	public HashMap<Item, Integer> getInventory(){return inventory;}
	
	private HashSet<Item> itemList = new HashSet<>();
	public HashSet<Item> getItems(){return itemList;}
	public void addItem(Item inItem) {itemList.add(inItem);}
	public void removeItem(Item outItem) {itemList.remove(outItem);}
	public void renameItem(String newName, String oldName) {
		Iterator<Item> thisItr = itemList.iterator();
		Item oldItem = getItem(oldName);
		while(thisItr.hasNext()) {
			Item thisItem = thisItr.next();
			//System.out.println("Comparing " + inItem.getName() + " with " + thisItem.getName());
			if(oldItem.getName().equalsIgnoreCase(thisItem.getName())) {
				oldItem.setName(newName);
			}else {
				//System.out.println("No match yet...");
			}
		}
	}
	
	public boolean hasItem(Item inItem) {
		Iterator<Item> thisItr = itemList.iterator();
		boolean hasItem = false;
		while(thisItr.hasNext()) {
			Item thisItem = thisItr.next();
			//System.out.println("Comparing " + inItem.getName() + " with " + thisItem.getName());
			if(inItem.getName().equalsIgnoreCase(thisItem.getName())) {
				hasItem = true;
			}else {
				//System.out.println("No match yet...");
			}
		}
		return hasItem;
	}
	
	private HashSet<Transaction>transactions = new HashSet<>();
	public void transaction(Transaction inTrans) {
		transactions.add(inTrans);
		calculate();
	}
	public void removeTransaction(Transaction inTrans) {
		transactions.remove(inTrans);
		calculate();
	}
	
	public Entry<Item, Integer> get(int row){
	    Iterator<Entry<Item, Integer>> invItr = inventory.entrySet().iterator();
	    Map.Entry<Item, Integer> outPair = invItr.next();
	    for(int i = 0; i < row; i++){
	        outPair = invItr.next();
	    }
	    return outPair;
	}
	
	public Item getItem(String itemName) {
		Iterator<Item> thisItr = itemList.iterator();
		Item outItem = null;
		while(thisItr.hasNext()) {
			Item thisItem = thisItr.next();
			//System.out.println("Comparing " + inItem.getName() + " with " + thisItem.getName());
			if(thisItem.getName().equalsIgnoreCase(itemName)) {
				outItem = thisItem;
			}else {
				//System.out.println("No match yet...");
			}
		}
		return outItem;
	}
	
	public int getNumber(Item inItem){return inventory.get(inItem);}
	
	public void parseLogs(Set<Log> logSet) {
		transactions.clear();
		Set<Log> newLogs =  logSet;
		Iterator<Log> logItr = newLogs.iterator();
		while(logItr.hasNext()) {
			Log thisLog = logItr.next();
			//System.out.println(thisLog.displayItems());
			Iterator<Transaction> transItr = thisLog.getTransactions().iterator();
			while(transItr.hasNext()){
				Transaction inTrans = transItr.next();
				transactions.add(inTrans);
				//System.out.println("Adding Transaction: " + inTrans.getItem().getName() + ": " + inTrans.getNumber());
			}
		}
		//System.out.println("Done adding transactions.");
		calculate();
	}
	
	public void calculate(){
		//System.out.println("==Begin Calculations==");
		Iterator<Transaction> transItr = transactions.iterator();
		inventory.clear();
		while(transItr.hasNext()) {
			boolean foundItem = false;
			Transaction inTrans = transItr.next();
			//System.out.println("Searching for Transaction: " + inTrans.getItem().getName() + "-" + inTrans.getNumber() + "-" + inTrans.getDir());
			switch(inTrans.getDir()){
			case IN:
				if(inventory.keySet().isEmpty()){
					inventory.put(inTrans.getItem(), inTrans.getNumber());
					//System.out.println("Inventory empty. Inserting: " + inTrans.getNumber() + " " + inTrans.getItem().getName());
				}else{
					for(Item item: inventory.keySet()){
						if(item.getName().equalsIgnoreCase(inTrans.getItem().getName())){
							//System.out.println(inTrans.getItem().getName() + " found. Beginning calculations.");
							int newVal = inventory.get(item);
							//System.out.println("Existing Values: " + inventory.get(item.getName()) + " " + item.getName());
							//System.out.println("Importing Values: " + inTrans.getNumber() + " " + inTrans.getItem().getName());
							newVal += inTrans.getNumber();
							//System.out.println("New Value: " + newVal);
							inventory.put(item, newVal);
							//System.out.println("Found.");
							foundItem = true;
						}
					}
					if(!foundItem){
						//System.out.println("Not Found.");
						inventory.put(inTrans.getItem(), inTrans.getNumber());
						//System.out.println(inTrans.getItem().getName() + " not found. Inserting " + inTrans.getNumber() + ".");
					}
				}
				
				
				break;
			case OUT:
				if(inventory.keySet().isEmpty()){
					inventory.put(inTrans.getItem(), inTrans.getNumber());
				}else{
					for(Item item: inventory.keySet()){
						if(item.getName().equalsIgnoreCase(inTrans.getItem().getName())){
							int newVal = inventory.get(item);
							newVal -= inTrans.getNumber();
							inventory.put(item, newVal);
							foundItem = true;
						}
					}
					if(!foundItem){
						inventory.put(inTrans.getItem(), inTrans.getNumber()*-1);
					}
				}
				break;
			case NULL:
				break;
			}
		}
	}
	
	public boolean checkForItem(Item inItem){
		boolean foundItem = false;
		for(Item item: inventory.keySet()){
			if(item.getName().equalsIgnoreCase(inItem.getName())){
				foundItem = true;
				return foundItem;
			}
		}
		return foundItem;
	}
	
	public void clearInv(){
		inventory.clear();
	}
}