package wliuso.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import wliuso.main.InventoryManager.InvModel;
import wliuso.main.InventoryManager.LogModel;

@SuppressWarnings("serial")
public class ItemWindow extends JFrame{
	
	int itemNum = 0;
	
	public ItemWindow(InvModel invModel, LogModel logModel, RecordFile inFile) {
		//For GUI
		JPanel itemPane = new JPanel(new GridBagLayout());
		setTitle("Add Item");
		setIconImage(RecordFile.omni);
		Image addImg = new ImageIcon(this.getClass().getResource("/icons/addRow.png")).getImage();
		GridBagConstraints labCons = new GridBagConstraints(); //Label Constraints
			labCons.gridx = 0; labCons.gridy = 0;
			labCons.insets = new Insets(5,5,5,5);
		GridBagConstraints inCons = new GridBagConstraints(); //Input Constraints
			inCons.gridy = 4; inCons.anchor = GridBagConstraints.CENTER; inCons.insets = new Insets(5,5,5,5);
		GridBagConstraints subCons = new GridBagConstraints(); //Submission Constraints
			subCons.gridx = 0; subCons.gridy = itemNum+7; subCons.insets = new Insets(5,5,5,5);
		
		//GUI for Item Edit
		JLabel dropName = new JLabel("Old Name");
		JLabel newName = new JLabel("New Name");
		Inventory inventory = inFile.getInventory();
		Set<Log> logList = inFile.getLogList();
		HashSet<Item> oldSet = inventory.getItems();
		Iterator<Item> oldItr = oldSet.iterator();
		JComboBox<String> oldItem = new JComboBox<String>();
		ArrayList<String> itemList = new ArrayList<>();
		while (oldItr.hasNext()) {
			itemList.add(oldItr.next().getName());
			//System.out.println(oldItr.next().getName());
		}
		java.util.Collections.sort(itemList);
		for(String item: itemList) {
			oldItem.addItem(item);
		}
		JTextField newItem = new JTextField(14);
		JButton editSubmit = new JButton("Submit");
		
		//Add Editing Components and Separator
		itemPane.add(dropName, labCons); labCons.gridx = 1;
		itemPane.add(newName, labCons); labCons.gridx = 0; labCons.gridy = 3;
		itemPane.add(oldItem, inCons); inCons.gridx = 1;
		itemPane.add(newItem, inCons); inCons.gridx = 2;
		itemPane.add(editSubmit, inCons); inCons.gridy = 5; inCons.gridx = 0;
		itemPane.add(new JSeparator(), inCons); inCons.gridy = 6;
		
		editSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
        		String oldName = oldItem.getSelectedItem().toString();
        		inventory.renameItem(newItem.getText(), oldName);
        		Iterator<Log> logItr = logList.iterator();
        		while(logItr.hasNext()) {
        			Log thisLog = logItr.next();
        			for(Transaction trans: thisLog.getTransactions()) {
        				Item thisItem = trans.getItem();
        				if(thisItem.getName().equalsIgnoreCase(oldName)) {
        					thisItem.setName(newItem.getText());
        				}
        			}
        		}
        		inFile.setLogList(logList);
        		inFile.setInventory(inventory);
        		inFile.save();
        		invModel.fireTableDataChanged();
        		logModel.fireTableDataChanged();
            }
        });
		
		//Add New Item(s) to Item List
		JLabel itemNa = new JLabel("Name");
		itemNa.setPreferredSize(new Dimension(80,25));
		JButton addItem = new JButton(new ImageIcon(addImg.getScaledInstance(16, 16, Image.SCALE_SMOOTH))); //Button to Add Additional Item Input Row
		addItem.setBorder(BorderFactory.createEmptyBorder());
		addItem.setContentAreaFilled(false);
		FlowLayout newLayout = new FlowLayout();
		newLayout.setAlignment(FlowLayout.RIGHT);
		itemNa.setLayout(newLayout); labCons.gridx = 0; labCons.gridy = 5;
		itemNa.add(addItem, newLayout);
		itemPane.add(itemNa, labCons);
		ArrayList<JComboBox<String>> itemBoxList = new ArrayList<>();
		newRow(inCons, itemPane, labCons, itemBoxList, addItem, inFile);
		JLabel success = new JLabel("");
		success.setMinimumSize(new Dimension(70,16));
		success.setPreferredSize(new Dimension(70,16));
		itemPane.add(success, subCons);
			subCons.gridx = 1;
		JButton addSubmit = new JButton("Submit");
		itemPane.add(addSubmit, subCons);
		addSubmit.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	        		for(JComboBox<String> item: itemBoxList){
	        			if(!item.getSelectedItem().equals("No Item")){
	        				Item thisItem = new Item(item.getSelectedItem().toString());
	        				//boolean
	        				if(inventory.hasItem(thisItem)){
	        					//Do Nothing
	        				}else {
	        					inventory.addItem(thisItem);
	        					//System.out.println(thisItem.getName() + " added!");
	        				}	
	        			}
	        		}
	        		inFile.setInventory(inventory);
	        		inFile.save();
            		SwingUtilities.getWindowAncestor(itemPane).dispose();
	            }
	        });

			addItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					itemPane.remove(addItem);
					itemPane.remove(success);
					itemPane.remove(addSubmit);
					newRow(inCons, itemPane, labCons, itemBoxList, addItem, inFile);
						subCons.gridx = 0; subCons.gridy = itemNum+7; subCons.insets = new Insets(5,5,5,5);
					itemPane.add(success, subCons);
						subCons.gridx = 1;
					itemPane.add(addSubmit, subCons);
						subCons.gridx = 2;
					itemPane.revalidate();
					itemPane.repaint();
					itemPane.getParent().setSize(750,750);
					SwingUtilities.getWindowAncestor(itemPane).pack();
					SwingUtilities.getWindowAncestor(itemPane).setMinimumSize(getPreferredSize());
				}
			});
			getContentPane().add(itemPane);
			SwingUtilities.getWindowAncestor(itemPane).pack();
			SwingUtilities.getWindowAncestor(itemPane).setMinimumSize(getPreferredSize());
		}
		
		public void newRow(GridBagConstraints inCons, JPanel itemPane, GridBagConstraints labCons, ArrayList<JComboBox<String>> itemBoxList, JButton addButton, RecordFile inFile) {
			itemNum += 1;
			int thisItem = itemNum;
				inCons.gridx = 0;
			JLabel addItem = new JLabel("Item " + thisItem);
			itemPane.add(addItem, inCons);
			labCons.anchor = GridBagConstraints.SOUTH;
				inCons.gridx = 1; inCons.insets = new Insets(0,0,0,10);
			JComboBox<String> itemBar = new JComboBox<>();
			itemBar.addItem("No Item");
			ArrayList<String> itemList = new ArrayList<>();
			for(Item item: inFile.getInventory().getItems()) {
				itemList.add(item.getName());
			}
			java.util.Collections.sort(itemList);
			for(String item: itemList) {
				itemBar.addItem(item);
			}
			itemBoxList.add(itemBar);
			itemPane.add(itemBar, inCons);
				inCons.gridy = itemNum+7;
		}

}
