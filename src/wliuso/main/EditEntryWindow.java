package wliuso.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import wliuso.main.InventoryManager.InvModel;
import wliuso.main.InventoryManager.LogModel;
import wliuso.main.Log.Direction;

@SuppressWarnings("serial")
public class EditEntryWindow extends JFrame {

	int itemNum = 0;
	
	public EditEntryWindow(InvModel invModel, LogModel logModel, Log editLog, RecordFile inFile) {
		JPanel editPane = new JPanel(new GridBagLayout());
		setTitle("Detail View");
		setIconImage(RecordFile.omni);
		
		//Gridbag Constraints for Alteration Section
		GridBagConstraints labels = new GridBagConstraints();
			labels.gridx = 0;labels.gridy = 0;
			labels.insets = new Insets(10,10,0,10);
			labels.anchor = GridBagConstraints.NORTH;
		GridBagConstraints entry = new GridBagConstraints();
			entry.gridx = 0;entry.gridy = 1;
			entry.anchor = GridBagConstraints.SOUTH;
		JLabel enterDir = new JLabel("In/Out");
		editPane.add(enterDir, labels);
			labels.gridx = 1;
		JLabel enterLocation = new JLabel("To/From");
		editPane.add(enterLocation, labels);
			labels.gridx = 2;
		JLabel enterDate = new JLabel("Date");
		editPane.add(enterDate, labels);
		String[] dirStrings = {"IN", "OUT"};
		JComboBox<String> dirBar = new JComboBox<String>(dirStrings);
		dirBar.setSelectedItem(editLog.getDirection().toString());
		dirBar.setEnabled(false);
		editPane.add(dirBar, entry);
			entry.gridx = 1;
		JTextField locationBar = new JTextField(20);
		locationBar.setText(editLog.getLocation());
		locationBar.setEditable(false);
		editPane.add(locationBar, entry);
			entry.gridx = 2;
		JTextField dateBar = new JTextField(10);
		dateBar.setText(editLog.getDate());
		dateBar.setEditable(false);
		editPane.add(dateBar, entry);
		GridBagConstraints labCons = new GridBagConstraints();
			labCons.gridx = 1; labCons.gridy = 3;
			labCons.insets = new Insets(20,10,0,10);
		JLabel itemNa = new JLabel("Name");
		editPane.add(itemNa, labCons);
			labCons.gridx = 2;
			
		//Button to Add Additional Item Input Row
		JButton addItem = new JButton(new ImageIcon(RecordFile.plusRow.getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
		addItem.setBorder(BorderFactory.createEmptyBorder());
		addItem.setContentAreaFilled(false);
		JLabel itemQu = new JLabel("Quantity");
		itemQu.setPreferredSize(new Dimension(80,25));
		FlowLayout newLayout = new FlowLayout();
		newLayout.setAlignment(FlowLayout.RIGHT);
		itemQu.setLayout(newLayout);
		itemQu.add(addItem, newLayout);
		addItem.setEnabled(false);
			
		editPane.add(itemQu, labCons);
		GridBagConstraints inCons = new GridBagConstraints();
			inCons.gridy = 4; labCons.anchor = GridBagConstraints.SOUTHEAST;
		ArrayList<JComboBox<String>> itemBoxList = new ArrayList<>();
		ArrayList<JTextField> quantityBoxList = new ArrayList<>();
		for(Transaction trans: editLog.getTransactions()){
			makeRow(inCons, editPane, labCons, itemBoxList, quantityBoxList, trans, inFile.getInventory());
			//System.out.println(itemNum);
		}
		GridBagConstraints subCons = new GridBagConstraints();
			subCons.gridx = 0; subCons.gridy = itemNum+5; subCons.insets = new Insets(10,10,10,10);
		JToggleButton editOn = new JToggleButton("Edit Entry");
		editPane.add(editOn, subCons);
		JButton submit = new JButton("Submit Changes");
		JButton delItem = new JButton("Delete Entry");
		submit.setEnabled(false);
		delItem.setEnabled(false);
		
		editOn.addActionListener((new ActionListener() {
			boolean isEditable = false;
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!isEditable){
					dirBar.setEnabled(true);
					locationBar.setEditable(true);
					dateBar.setEditable(true);
					addItem.setEnabled(true);
					for(JComboBox<String> itemBox: itemBoxList){
						itemBox.setEnabled(true);
					}
					for(JTextField quantityBox: quantityBoxList){
						quantityBox.setEditable(true);
					}
					submit.setEnabled(true);
					delItem.setEnabled(true);
					isEditable = true;
				}else{
					dirBar.setEnabled(false);
					locationBar.setEditable(false);
					dateBar.setEditable(false);
					addItem.setEnabled(false);
					for(JComboBox<String> itemBox: itemBoxList){
						itemBox.setEnabled(false);
					}
					for(JTextField quantityBox: quantityBoxList){
						quantityBox.setEditable(false);
					}
					submit.setEnabled(false);
					delItem.setEnabled(false);
					isEditable= false;
				}
			}
		}));
			subCons.gridx = 1;
		editPane.add(submit, subCons);
		submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	boolean validInts = true;
            	for(JTextField inField: quantityBoxList){
            		try{Integer.parseInt(inField.getText());}
            		catch (NumberFormatException heck){
    					JOptionPane.showMessageDialog(null, "Error! There is non-integer text in a quantity box.");
    					validInts = false;
    				}
            	}
            	if(validInts){
                	Log thisLog = new Log(Direction.valueOf(dirBar.getSelectedItem().toString()), locationBar.getText(), dateBar.getText());
            		for(int i = 0; i < itemBoxList.size(); i++){
            			Transaction thisTrans = new Transaction(Direction.valueOf(dirBar.getSelectedItem().toString()), new Item(itemBoxList.get(i).getSelectedItem().toString()), Integer.parseInt(quantityBoxList.get(i).getText()));
            			thisLog.addTransaction(thisTrans);
            			inFile.getInventory().transaction(thisTrans);
            		}
            		inFile.getLogList().remove(editLog);
            		inFile.getLogList().add(thisLog);
            		inFile.getInventory().parseLogs(inFile.getLogList());
            		Set<Item> outItemList = inFile.getInventory().getInventory().keySet();
            		invModel.clear();
            		for(Item item: outItemList){
            			ArrayList<Object> workRow = new ArrayList<>();
            			workRow.add(item.getName());
            			workRow.add(inFile.getInventory().getNumber(item));
            			invModel.addRow(workRow.toArray());
            		}
            		inFile.setLogList(inFile.getLogList());
            		inFile.save();
            		invModel.fireTableDataChanged();
            		logModel.fireTableDataChanged();
            		SwingUtilities.getWindowAncestor(editPane).dispose();
            	}
            }
        });
		
		addItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editPane.remove(addItem);
				editPane.remove(editOn);
				editPane.remove(submit);
				editPane.remove(delItem);
				newRow(inCons, editPane, labCons, itemBoxList, quantityBoxList, addItem, inFile.getInventory());
					subCons.gridx = 0; subCons.gridy = itemNum+5; subCons.insets = new Insets(10,10,10,10);
				editPane.add(editOn, subCons);
					subCons.gridx = 1;
				editPane.add(submit, subCons);
					subCons.gridx = 2;
				editPane.add(delItem, subCons);
				editPane.revalidate();
				editPane.repaint();
				editPane.getParent().setSize(750,750);
				SwingUtilities.getWindowAncestor(editPane).pack();
				SwingUtilities.getWindowAncestor(editPane).setMinimumSize(getPreferredSize());
			}
		});
		
		delItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(delItem, "Delete Entry", "Are you sure you want to delete this entry?", JOptionPane.YES_NO_OPTION);
				switch(confirmed){
				case 0:
					inFile.getLogList().remove(editLog);
					inFile.getInventory().parseLogs(inFile.getLogList());
            		Set<Item> outItemList = inFile.getInventory().getInventory().keySet();
            		invModel.clear();
            		for(Item item: outItemList){
            			ArrayList<Object> workRow = new ArrayList<>();
            			workRow.add(item.getName());
            			workRow.add(inFile.getInventory().getNumber(item));
            			invModel.addRow(workRow.toArray());
            		}
					logModel.clear();
					Iterator<Log> logItr = inFile.getLogList().iterator();
					while(logItr.hasNext()) {
						Log thisLog = logItr.next();
						ArrayList<Object> workRow = new ArrayList<>();
            			workRow.add(thisLog.getDate());
            			workRow.add(thisLog.getLocation());
            			workRow.add(thisLog.displayItems());
            			workRow.add(thisLog.getDirection());
            			logModel.addRow(workRow.toArray());
					}
					inFile.setLogList(inFile.getLogList());
					inFile.save();
					invModel.fireTableDataChanged();
	        		logModel.fireTableDataChanged();
	        		SwingUtilities.getWindowAncestor(editPane).dispose();
					break;
				case 1:
					//No Action
					break;
				default:
					System.out.println("How did you get this option?");
					break;
				}
			}
		});
		
		subCons.gridx = 2;
		editPane.add(delItem, subCons);
		getContentPane().add(editPane);
		SwingUtilities.getWindowAncestor(editPane).pack();
		SwingUtilities.getWindowAncestor(editPane).setMinimumSize(getPreferredSize());
	}
	
	private void makeRow(GridBagConstraints inCons, JPanel addPane, GridBagConstraints labCons, ArrayList<JComboBox<String>> itemBoxList, ArrayList<JTextField> quantityBoxList, Transaction trans, Inventory inventory) {
		itemNum += 1;
		int thisItem = itemNum;
			inCons.gridx = 0;
		JLabel addItem = new JLabel("Item " + thisItem);
		addPane.add(addItem, inCons);
		labCons.anchor = GridBagConstraints.SOUTH;
			inCons.gridx = 1;
		JComboBox<String> itemBar = new JComboBox<String>();
		itemBar.addItem("No Item");
		ArrayList<String> itemList = new ArrayList<>();
		for(Item item: inventory.getItems()) {
			itemList.add(item.getName());
		}
		java.util.Collections.sort(itemList);
		int itemVal = 0;
		for(String item: itemList) {
			itemBar.addItem(item);
			if(item.equalsIgnoreCase(trans.getItem().getName())){
				itemBar.setSelectedIndex(itemVal+1);
			}
			itemVal++;
		}
		
		itemBar.setEditable(false);
		itemBoxList.add(itemBar);
		addPane.add(itemBar, inCons);
			inCons.gridx = 2;
		JTextField quantityBar = new JTextField(4);
		quantityBar.setText(Integer.toString(trans.getNumber()));
		quantityBar.setEditable(false);
		quantityBoxList.add(quantityBar);
		addPane.add(quantityBar, inCons);
			inCons.gridy = itemNum+5;
	}
	
	public void newRow(GridBagConstraints inCons, JPanel editPane, GridBagConstraints labCons, ArrayList<JComboBox<String>> itemBoxList, ArrayList<JTextField> quantityBoxList, JButton addButton, Inventory inventory) {
		itemNum += 1;
		int thisItem = itemNum;
			inCons.gridx = 0;
		JLabel addItem = new JLabel("Item " + thisItem);
		editPane.add(addItem, inCons);
		labCons.anchor = GridBagConstraints.SOUTH;
			inCons.gridx = 1;
		JComboBox<String> itemBar = new JComboBox<>();
		itemBar.addItem("No Item");
		ArrayList<String> itemList = new ArrayList<>();
		for(Item item: inventory.getItems()) {
			itemList.add(item.getName());
		}
		java.util.Collections.sort(itemList);
		for(String item: itemList) {
			itemBar.addItem(item);
		}
		itemBoxList.add(itemBar);
		editPane.add(itemBar, inCons);
			inCons.gridx = 2;
		JTextField quantityBar = new JTextField(4);
		quantityBoxList.add(quantityBar);
		editPane.add(quantityBar, inCons);
			inCons.gridy = itemNum+5;
	}
}
