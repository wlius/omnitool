package wliuso.main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

@SuppressWarnings("serial")
public class FileWindow extends JFrame{
	
	public FileWindow(RecordFile inFile, JMenuBar outMenu){
		JPanel saveOut = new JPanel(new GridBagLayout());
		setTitle("File Settings");
		setIconImage(RecordFile.omni);
		GridBagConstraints saveCons = new GridBagConstraints();
		saveCons.anchor = GridBagConstraints.CENTER;
		saveCons.insets = new Insets(5,5,5,5);
		JLabel recFile = new JLabel("Record File");
		saveOut.add(recFile, saveCons);
			saveCons.gridx = 1;
		JTextField recField = new JTextField(15);
		recField.setText(inFile.getRecord());
		saveOut.add(recField, saveCons);
			saveCons.gridx = 2;
		JButton recPick = new JButton("Browse");
		saveOut.add(recPick, saveCons);
			saveCons.gridy = 3;

		recPick.addActionListener(new ActionListener() {
			@Override
	        public void actionPerformed(ActionEvent e) {
				JFileChooser recChoose = new JFileChooser(System.getProperty("user.home"));
				FileNameExtensionFilter xml = new FileNameExtensionFilter("Wireless Links Record File", "xml");
				recChoose.addChoosableFileFilter(xml);
				int returnVal = recChoose.showOpenDialog(FileWindow.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					java.io.File newRec = recChoose.getSelectedFile();
					recField.setText(newRec.getPath());
				}
			}
		});
		
		JButton submit = new JButton("Submit");
		saveOut.add(submit, saveCons);
		submit.addActionListener(new ActionListener() {
			@Override
	        public void actionPerformed(ActionEvent e) {
				String recPath = recField.getText();
				inFile.setRecord(recPath);
				inFile.load();
				SwingUtilities.getWindowAncestor(saveOut).dispose();
			}
		});
		
		
		saveOut.setMinimumSize(getPreferredSize());
		getContentPane().add(saveOut);
		SwingUtilities.getWindowAncestor(saveOut).pack();
		SwingUtilities.getWindowAncestor(saveOut).setMinimumSize(getPreferredSize());
	}

}
